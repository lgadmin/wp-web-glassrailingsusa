<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<?php

		$cta_title  = get_field( 'footer_cta', 'option' )['title'];
		$cta_button = get_field( 'footer_cta', 'option' )['button'];

	if ( $post->post_type != 'service' && $post->post_type != 'product' ) {
		include locate_template( '/templates/template-parts/footer/cta-block.php' );
	}

	?>

	<footer id="colophon" class="site-footer text-center text-md-left">
		
		<div id="site-footer" class="py-4">
			<div class="container-fluid footer-main" >
				<div class="logo">
					<?php echo has_custom_logo() ? get_custom_logo() : do_shortcode( '[lg-site-logo]' ); ?>
				</div>
				<div class="footer-call">
					<span class="text-primary font-weight-bold">CALL US</span> <a class="text-secondary" href="tel:<?php echo the_field( 'contact_1_phone', 'options' ); ?>"><span> <?php echo the_field( 'contact_1_name', 'options' ); ?></span> <span><?php echo the_field( 'contact_1_phone', 'options' ); ?></span></a> <span class="h4"> | </span>
					<a class="text-secondary" href="tel:<?php echo the_field( 'contact_2_phone', 'options' ); ?>"><span><?php echo the_field( 'contact_2_name', 'options' ); ?></span> <span><?php echo the_field( 'contact_2_phone', 'options' ); ?> </span></a> <span class=""> | </span>
					<a class="text-secondary" href="tel:<?php echo the_field( 'contact_3_phone', 'options' ); ?>"><span><?php echo the_field( 'contact_3_name', 'options' ); ?></span> <span><?php echo the_field( 'contact_3_phone', 'options' ); ?> </span></a> <span class=""> | </span>
					<a class="text-secondary" href="tel:<?php echo the_field( 'contact_4_phone', 'options' ); ?>"><span><?php echo the_field( 'contact_4_name', 'options' ); ?></span> <span><?php echo the_field( 'contact_4_phone', 'options' ); ?> </span></a><br>
					<div class="line2"><span class="text-primary font-weight-bold">EMAIL US</span> <a class="text-secondary" href="mailto:<?php echo the_field( 'sales_email', 'options' ); ?>"><?php echo the_field( 'sales_email', 'options' ); ?> </a>
					<span class="text-primary font-weight-bold">CONNECT WITH US</span> <?php echo do_shortcode( '[lg-social-media]' ); ?></div>
				</div>
			</div>
			<div class="container-fluid footer-mobile-main">
			<div class="logo">
					<?php echo has_custom_logo() ? get_custom_logo() : do_shortcode( '[lg-site-logo]' ); ?>
			</div>
			<div class="footer-call">
				<div class="footer-lineheader">CALL US</div>
				<div>
					<a class="text-secondary" href="tel:<?php echo the_field( 'contact_1_phone', 'options' ); ?>"><span><?php echo the_field( 'contact_1_name', 'options' ); ?></span><span><?php echo the_field( 'contact_1_phone', 'options' ); ?></span></a><br>
					<a class="text-secondary" href="tel:<?php echo the_field( 'contact_2_phone', 'options' ); ?>"><span><?php echo the_field( 'contact_2_name', 'options' ); ?></span><span><?php echo the_field( 'contact_2_phone', 'options' ); ?></span></a><br>
					<a class="text-secondary" href="tel:<?php echo the_field( 'contact_3_phone', 'options' ); ?>"><span><?php echo the_field( 'contact_3_name', 'options' ); ?></span><span><?php echo the_field( 'contact_3_phone', 'options' ); ?></span></a><br>
					<a class="text-secondary" href="tel:<?php echo the_field( 'contact_4_phone', 'options' ); ?>"><span><?php echo the_field( 'contact_4_name', 'options' ); ?></span><span><?php echo the_field( 'contact_4_phone', 'options' ); ?></span></a>
				</div>	
				<div class="footer-lineheader">EMAIL US</div>
				<div>
					 <a class="text-secondary" href="mailto:<?php echo the_field( 'sales_email', 'options' ); ?>"><?php echo the_field( 'sales_email', 'options' ); ?> </a>
				</div>
				<div class="footer-lineheader last-line">CONNECT WITH US</div>
				<div>
					<?php echo do_shortcode( '[lg-social-media]' ); ?>
				</div>

		</div>
		</div>

		<div id="site-legal">
			<div class="container py-2">
				<div class="site-info"><?php get_template_part( '/templates/template-parts/footer/site-info' ); ?></div>
				<div class="site-longevity"> <?php get_template_part( '/templates/template-parts/footer/site-footer-longevity' ); ?> </div>
			</div>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
