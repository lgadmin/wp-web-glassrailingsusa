<?php 
/**
 * Grid List Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	// Block Fields
	$block_title = get_sub_field('block_title');
	$items = get_sub_field('grid_list');
	$item_per_row = get_sub_field('item_per_row');

	$item_per_row_lg  = get_sub_field('item_per_row_lg');
	$item_per_row_md  = $item_per_row;
	$item_per_row_sm  = get_sub_field('item_per_row_sm');
	$item_per_row_xs  = get_sub_field('item_per_row_xs');

	$item_per_row_lg = (!empty($item_per_row_lg) ? $item_per_row_lg : $item_per_row);
	$item_per_row_sm = (!empty($item_per_row_sm) ? $item_per_row_sm :  6 );
	$item_per_row_xs = (!empty($item_per_row_xs) ? $item_per_row_xs : 12 );

?>

<?php if($items && is_array($items)): ?>
	<?php foreach ($items as $key => $value): ?>
		<div class="col col-<?php echo 12 / $item_per_row_xs; ?> col-sm-<?php echo 12 / $item_per_row_sm; ?> col-md-<?php echo 12 / $item_per_row; ?> col-lg-<?php echo 12 / $item_per_row_lg; ?> d-flex flex-column"><?php echo $value['content']; ?></div>
	<?php endforeach; ?>
<?php endif; ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>