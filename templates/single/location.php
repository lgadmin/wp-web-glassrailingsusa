<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php
					$phone = get_field('phone');
					$email = get_field('email');
					$map_image = get_field('map_image');
					$description = get_field('description');
				?>

				<div class="py-4 container">
					<div class="split-content-location">
						<div class="split-image">
							<img src="<?php echo $map_image['url']; ?>" alt="<?php echo $map_image['alt']; ?>">
						</div>

						<div class="split-copy">


							<h1 class="text-secondary h2">
								Glass Railings <?php the_title(); ?>
							</h1>
							<div class="py-2">

								<?php while ( have_rows('location') ) : the_row(); ?>
									
									<?php $location_title = get_sub_field('title'); ?>

									<?php if($location_title): ?>
										<div class="h4 py-2">
											 <?php echo get_sub_field('title'); ?>
										</div>
									<?php endif ?>


									<?php if( have_rows('persons') ): ?>
										<?php while ( have_rows('persons') ) : the_row(); ?>
											<div class="h5">
												<?php echo get_sub_field('person'); ?>
											</div>
										<?php endwhile ?>
									<?php endif ?>
									
									<?php if( have_rows('phones') ): ?>
										<?php while ( have_rows('phones') ) : the_row(); ?>
											<div class="h5">
												P: <a href="tel:<?php echo get_sub_field('phone'); ?>"><?php echo get_sub_field('phone'); ?></a>
											</div>
										<?php endwhile ?>
									<?php endif ?>

									<?php if( have_rows('cells') ): ?>
										<?php while ( have_rows('cells') ) : the_row(); ?>
											<div class="h5">
												C: <a href="tel:<?php echo get_sub_field('cell'); ?>"><?php echo get_sub_field('cell'); ?></a>
											</div>
										<?php endwhile ?>
									<?php endif ?>

									<?php if( have_rows('faxes') ): ?>
										<?php while ( have_rows('faxes') ) : the_row(); ?>
											<div class="h5">
												P: <a href="tel:<?php echo get_sub_field('fax'); ?>"><?php echo get_sub_field('fax'); ?></a>
											</div>
										<?php endwhile ?>
									<?php endif ?>
									
									<?php if( have_rows('emails') ): ?>
										<?php while ( have_rows('emails') ) : the_row(); ?>
											<div class="h5">
												E: <a href="mailto:<?php echo get_sub_field('email'); ?>"><?php echo get_sub_field('email'); ?></a>
											</div>
										<?php endwhile ?>
									<?php endif ?>
									
									<?php if( have_rows('websites') ): ?>
										<?php while ( have_rows('websites') ) : the_row(); ?>
											<div class="h5">
												W: <a href="<?php echo get_sub_field('website'); ?>"><?php echo get_sub_field('website'); ?></a>
											</div>
										<?php endwhile ?>
									<?php endif ?>

									<?php if( have_rows('addresses') ): ?>
										<?php while ( have_rows('addresses') ) : the_row(); ?>
											<div class="h5">
												<?php
													$address_1 = get_sub_field('address_1');
													$address_2 = get_sub_field('address_2');
												?>

												<?php if($address_1): ?>
													<?php echo get_sub_field('address_1'); ?>
												<?php endif ?>

												<?php if($address_2): ?>
													<?php echo get_sub_field('address_2'); ?>
												<?php endif ?>
											</div>
										<?php endwhile ?>
									<?php endif ?>
	
								<?php endwhile; ?>

							</div>



							<div>
								If you would like to become a Falcon Railings dealer, please fill out our <a href="/request-dealer-installers/">Request for Dealer/Installers Form</a>.
							</div>


						</div>
					</div>
				</div>

				<div class="py-4 bg-light-colour">
					<div class="container">
						<?php if($description): ?>
							<?php the_field('description'); ?>
						<?php else: ?>
							<?php the_field('location_description', 'option'); ?>
						<?php endif; ?>
					</div>	
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>