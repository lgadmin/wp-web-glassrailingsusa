<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				<?php if ( have_posts() ) : ?>
					<?php woocommerce_content(); ?>
				<?php endif ?>
			</main>
		</div>
	</div>

<?php get_footer(); ?>