<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php get_template_part( '/components/acf-flexible-layout/layouts' ); ?>

				<?php get_template_part( '/templates/template-parts/page/our-products' ); ?>

			</main>
		</div>
	</div>

<?php get_footer(); ?>