<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container py-5">
				<?php while (have_posts()) : the_post(); ?>

				  <?php the_content(); ?>

				<?php endwhile; ?>
			</main>
		</div>
	</div>

<?php get_footer(); ?>