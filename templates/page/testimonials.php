<?php
get_header(); ?>

    <div id="primary">
        <div id="content" role="main" class="site-content">
            <main>

                <div class="container py-4">
                    <h1 class="h2 mb-4 text-center"><strong>Testimonials</strong></h1>
                    <?php
                        $args = array(
                            'showposts' => -1,
                            'post_type'     => 'lg_testimonial',
                        );

                        $result = new WP_Query( $args );

                        // Loop
                        if ( $result->have_posts() ) :
                            ?>
                            
                            <div class="our-testimonials-loop">
                            <?php
                            while( $result->have_posts() ) : $result->the_post(); 
                            $author = get_field('author');
                            $content = get_field('content');
                            ?>
                                <div>
                                    <div class="testimonial-author small mb-2">- <?php echo $author; ?></div>
                                    <div class="testimonial-content">
                                        <?php echo $content; ?>
                                    </div>
                                </div>
                            <?php
                            endwhile;
                            ?>
                            </div>
                        <?php

                        endif; // End Loop

                        wp_reset_query();
                    ?>
                </div>
                
            </main>
        </div>
    </div>

<?php get_footer(); ?>