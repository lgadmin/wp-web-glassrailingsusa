<?php

	$long_description = get_field('long_description');
	$features = get_field('features');
	$shipping_info = get_field('shipping_info');

?>

<div id="single-product-details">
	<ul class="nav nav-tabs container d-flex flex-wrap align-items-center text-uppercase" id="product-details" role="tablist">
	<?php if($long_description): ?>
	  <li class="nav-item mr-1">
	    <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
	  </li>
	<?php endif; ?>
	<?php if($features): ?>
	  <li class="nav-item mr-1">
	    <a class="nav-link" id="features-tab" data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="false">Features</a>
	  </li>
	<?php endif; ?>
	  <!--<li class="nav-item mr-1">
	    <a class="nav-link" id="customer-review-tab" data-toggle="tab" href="#customer-review" role="tab" aria-controls="customer-review" aria-selected="false">Customer Reviews</a>
	  </li>-->
	<?php if($shipping_info): ?>
	  <li class="nav-item mr-1">
	    <a class="nav-link" id="shipping-info-tab" data-toggle="tab" href="#shipping-info" role="tab" aria-controls="shipping-info" aria-selected="false">Shipping Info</a>
	  </li>
	<?php endif; ?>
	</ul>
	<div class="tab-content" id="product-details-content">
	<?php if($long_description): ?>
	  <div class="tab-pane fade show active bg-light-colour py-4" id="description" role="tabpanel" aria-labelledby="description-tab">
	  	<div class="container">
	  		<?php echo $long_description; ?>
	  	</div>
	  </div>
	<?php endif; ?>
	<?php if($features): ?>
	  <div class="tab-pane fade bg-light-colour py-4" id="features" role="tabpanel" aria-labelledby="features-tab">
	  	<div class="container">
	  		<?php echo $features; ?>
	  	</div>
	  </div>
	<?php endif; ?>
	  <!--<div class="tab-pane fade bg-light-colour py-4" id="customer-review" role="tabpanel" aria-labelledby="customer-review-tab">
	  	<div class="container">
	  		<?php //comments_template( 'woocommerce/single-product/review' ); ?>
	  	</div>
	  </div>-->
	  <?php if($shipping_info): ?>
	  <div class="tab-pane fade bg-light-colour py-4" id="shipping-info" role="tabpanel" aria-labelledby="shipping-info-tab">
	  	<div class="container">
	  		<?php echo $shipping_info; ?>
	  	</div>
	  </div>
	<?php endif; ?>
	</div>
</div>