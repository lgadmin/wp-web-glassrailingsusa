<?php

	$feature_slider_active = get_field('feature_slider_active');
	$form_active = get_field('form_active');

?>

<?php

if( have_rows('feature_slider') && $feature_slider_active == 1 ):
	?>
		<div class="feature-slider-wrap">
			<div class="feature-slider">
				<?php
				    while ( have_rows('feature_slider') ) : the_row();
				        $image = get_sub_field('image');
				        ?>
				        <div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
				        <?php
				    endwhile;
			    ?>
		    </div>

		    <?php if($form_active): ?>
			    <div class="overlay_wrap py-4 d-none d-sm-block">
				    <div class="overlay container">
					    <?php if($form_active): ?>
					    	<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
					    <?php endif; ?>
				    </div>
			    </div>
			<?php endif; ?>

	    </div>
    <?php
else :
    // no rows found
endif;

?>