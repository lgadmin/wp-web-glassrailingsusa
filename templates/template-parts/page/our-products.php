

<div class="py-4 our-products bg-primary">
	<div class="container">
		<h3 class="h2 text-center text-white mb-4">Our Products</h3>

		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'product',
		    );

		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	
				<div class="our-product-loop row justify-content-start justify-content-md-center">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post(); 
		    	$title = get_the_title();
		    	$link = get_permalink();
		    	$thumbnail = get_the_post_thumbnail_url();
		    ?>
		    	
		        <div class="text-center text-white col-sm-6 col-md-6 col-lg-4 my-2 px-sm-4">
		        	<img src="<?php echo $thumbnail; ?>" class="img-full">
		        	<p class="my-2"><?php echo $title; ?></p>
		        	<a href="<?php echo $link; ?>" class="btn btn-sm btn-secondary">Buy Now</a>
		        </div>

				<?php
		        endwhile;
		        ?>
		        </div>
		    <?php

		    endif; // End Loop

		    wp_reset_query();
		?>

	</div>
</div>