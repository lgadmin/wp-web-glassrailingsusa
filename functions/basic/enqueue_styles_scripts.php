<?php

/* BEGIN CSS FILE */

global $lg_styles, $lg_scripts;

$lg_styles = [
	(object) array(
		"handle" => "lg-fonts",
		"src" => 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800|PT+Sans:400,400i,700,700i',
		"dependencies" => [],
		"version" => false
	),
	(object) array(
		"handle" => "lg-style",
		"src" => get_stylesheet_directory_uri() . '/style.css',
		"dependencies" => [],
		"version" => filemtime(get_template_directory() . '/style.css')
	),
	(object) array(
		"handle" => "lg-style-vendor",
		"src" => get_stylesheet_directory_uri() . '/assets/dist/css/vendor.min.css',
		"dependencies" => [],
		"version" => filemtime(get_template_directory() . '/assets/dist/css/vendor.min.css')
	)
];

/* END CSS FILE */

/* BEGIN JS FILE */

$lg_scripts = [
	(object) array(
		"handle" => "lg-script",
		"src" => get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js",
		"dependencies" => array('jquery'),
		"version" => filemtime(get_template_directory() . "/assets/dist/js/script.min.js")
	),
	(object) array(
		"handle" => "lg-script-vendor",
		"src" =>  get_stylesheet_directory_uri() . "/assets/dist/js/vendor.min.js",
		"dependencies" => array('jquery'),
		"version" => filemtime(get_template_directory() . "/assets/dist/js/vendor.min.js")
	),
];
/* END JS FILE */

function lg_enqueue_styles_scripts() {

	global $lg_styles, $lg_scripts;

	/* INCLUDE MAIN */

	if($lg_styles && is_array($lg_styles)){
		foreach ($lg_styles as $key => $style) {

			wp_enqueue_style(
				$style->handle,
				$style->src,
				$style->dependencies,
				$style->version
			);
		}
	}

	if($lg_scripts && is_array($lg_scripts)){
		foreach ($lg_scripts as $key => $script) {
			wp_enqueue_script(
				$script->handle,
				$script->src,
				$script->dependencies,
				$script->version
			);

			wp_enqueue_script( $script->handle );
		}
	}

	/* END INCLUDE MAIN */

}

add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


if($lg_styles && is_array($lg_styles)){
	foreach ($lg_styles as $key => $style) {
		add_editor_style($style->src);
	}
}

?>