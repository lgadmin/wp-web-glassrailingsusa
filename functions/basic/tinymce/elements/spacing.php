<?php

	global $lg_tinymce_margin_top;
	global $lg_tinymce_margin_bottom;

	$margin_top = [];
	$margin_bottom = [];

	$spacing = array(
		(object) array("title"=> "No Spacing", "value"=> "0"),
		(object) array("title"=> "Extra Small", "value"=> "1"),
		(object) array("title"=> "Small", "value"=> "2"),
		(object) array("title"=> "Medium", "value"=> "3"),
		(object) array("title"=> "Large", "value"=> "4"),
		(object) array("title"=> "Extra Large", "value"=> "5")
	);

	if($spacing && is_array($spacing)){
		foreach ($spacing as $key => $value) {
			array_push($margin_top, array(
				'title' => $value->title,
	            'selector' => 'div,h1,h2,h3,h4,h5,h6,p,a,ul,ol,blockquote',
	            'classes' => 'mt-'.$value->value
			));

			array_push($margin_bottom, array(
				'title' => $value->title,
	            'selector' => 'div,h1,h2,h3,h4,h5,h6,p,a,ul,ol,blockquote',
	            'classes' => 'mb-'.$value->value
			));
		}
	}

	$lg_tinymce_margin_top = array(
        'title' => 'Margin Top',
        'items' =>  $margin_top
    );

    $lg_tinymce_margin_bottom = array(
        'title' => 'Margin Bottom',
        'items' =>  $margin_bottom
    );

?>