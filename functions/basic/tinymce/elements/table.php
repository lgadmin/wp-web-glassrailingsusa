<?php

	global $lg_tinymce_table;
    
    $lg_tinymce_table = array(
        'title' => 'Table',
        'items' =>  array(
			array(
				'title' => 'Table',
				'selector' => 'table',
				'classes'	=> 'table'
			),
			array(
				'title' => 'Table Dark',
				'selector' => 'table',
				'classes'	=> 'table-dark'
			),
			array(
				'title' => 'Table Striped',
				'selector' => 'table',
				'classes'	=> 'table-striped'
			),
			array(
				'title' => 'Table Hover',
				'selector' => 'table',
				'classes'	=> 'table-hover'
			),
			array(
				'title' => 'Table Border',
				'selector' => 'table',
				'classes'	=> 'table-bordered'
			),
			array(
				'title' => 'Table No Border',
				'selector' => 'table',
				'classes'	=> 'table-borderless'
			),
			array(
				'title' => 'Table Responsive XS',
				'selector' => 'table',
				'classes'	=> 'table-responsive'
			),
			array(
				'title' => 'Table Responsive SM',
				'selector' => 'table',
				'classes'	=> 'table-responsive-sm'
			),
			array(
				'title' => 'Table Responsive MD',
				'selector' => 'table',
				'classes'	=> 'table-responsive-md'
			),
			array(
				'title' => 'Table Responsive LG',
				'selector' => 'table',
				'classes'	=> 'table-responsive-lg'
			),
			array(
				'title' => 'Table Responsive XL',
				'selector' => 'table',
				'classes'	=> 'table-responsive-xl'
			)
        )
    );

?>