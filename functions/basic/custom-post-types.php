<?php

    // SERVICE
    
    function lg_custom_post_type(){
      register_post_type( 'location',
          array(
            'labels' => array(
              'name' => __( 'Locations' ),
              'singular_name' => __( 'Location' )
            ),
            'public' => true,
            'has_archive' => true,
            'menu_icon'   => 'dashicons-location',
            "rewrite" => array("with_front" => false, "slug" => 'locations'),
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );
      register_post_type( 'service',
          array(
            'labels' => array(
              'name' => __( 'Services' ),
              'singular_name' => __( 'Service' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );

      register_post_type( 'color',
          array(
            'labels' => array(
              'name' => __( 'Product Colors' ),
              'singular_name' => __( 'Product Color' )
            ),
            'public' => false,
            'has_archive' => false,
            'publicly_queryable'  => true,
            'show_ui' => true,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'edit.php?post_type=product',
          )
      );

      register_post_type( 'shape',
          array(
            'labels' => array(
              'name' => __( 'Product Shapes' ),
              'singular_name' => __( 'Product Shape' )
            ),
            'public' => false,
            'has_archive' => false,
            'publicly_queryable'  => true,
            'show_ui' => true,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'edit.php?post_type=product',
          )
      );

      register_post_type( 'length',
          array(
            'labels' => array(
              'name' => __( 'Product Length' ),
              'singular_name' => __( 'Product Length' )
            ),
            'public' => false,
            'has_archive' => false,
            'publicly_queryable'  => true,
            'show_ui' => true,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'edit.php?post_type=product',
          )
      );

      register_post_type( 'cap',
          array(
            'labels' => array(
              'name' => __( 'Product Caps' ),
              'singular_name' => __( 'Product Cap' )
            ),
            'public' => false,
            'has_archive' => false,
            'publicly_queryable'  => true,
            'show_ui' => true,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'edit.php?post_type=product',
          )
      );

      register_post_type( 'use',
          array(
            'labels' => array(
              'name' => __( 'Product Use' ),
              'singular_name' => __( 'Product Use' )
            ),
            'public' => false,
            'has_archive' => false,
            'publicly_queryable'  => true,
            'show_ui' => true,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'edit.php?post_type=product',
          )
      );

      register_post_type( 'surface',
          array(
            'labels' => array(
              'name' => __( 'Product Surface' ),
              'singular_name' => __( 'Product Surface' )
            ),
            'public' => false,
            'has_archive' => false,
            'publicly_queryable'  => true,
            'show_ui' => true,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'edit.php?post_type=product',
          )
      );

    }

    add_action( 'init', 'lg_custom_post_type' );

?>