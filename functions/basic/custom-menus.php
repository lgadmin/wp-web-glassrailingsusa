<?php

function lg_custom_menu() {
    add_menu_page(
        'Glass Railings',
        'Glass Railings',
        'edit_posts',
        'lg_menu',
        '',
        'dashicons-admin-site',
        2
    );

    add_submenu_page(
        'lg_menu',
        __('Client Instruction'), 
        __('Client Instruction'), 
        'edit_posts', 
        'client-instruction', 
        'lg_client_instruction'
    );

    if( function_exists('acf_add_options_page') ) {
		$option_page = acf_add_options_page(array(
			'page_title' 	=> 'Global Theme Options',
			'menu_title' 	=> 'Global Theme Options',
			'menu_slug' 	=> 'global-theme-options',
			'parent_slug'	=> 'lg_menu',
			'capability' 	=> 'edit_posts',
			'redirect' 	=> false
		));
	}
}

add_action( 'admin_menu', 'lg_custom_menu' );

function lg_client_instruction(){
    require_once(get_stylesheet_directory() . '/functions/basic/client-instruction.php');
}

?>