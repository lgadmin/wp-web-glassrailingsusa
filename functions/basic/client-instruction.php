<style>
	ul{
		list-style:default;
		padding-left:20px;
	}
</style>

<div>
	<h1>Client Instruction</h1>

	<div style="padding: 15px; background-color:#fff;">
		
		<div>
			<h2>Site General</h2>
			<ul>
				<li><b>Logo:</b> LG Theme -> Site Settings -> Logo</li>
				<li><b>Social Media:</b> LG Theme -> Social Media</li>
				<li><b>Contact Info:</b> Dashboard-> LG Theme -> Contact</li>
				<li><b>Tracking Scripts:</b> LG Theme -> Analytics Tracking (Google tag manager setup under Longevity Account, No code injection to the header)</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Pages</h2>
			<ul>
				<li><b>Home:</b> Pages -> Home</li>
				<li><b>About:</b> Pages -> About</li>
			</ul>
		</div>

		<div>
			<h2>Locations Colours</h2>
			<ul>
				<li><b>Global Theme Options</b>: Location Description - Replace the Image file</li>
			</ul>
		</div>


	</div>

</div>