<?php

	// ---- Product Options

	add_action( 'woocommerce_before_add_to_cart_button', 'lg_woocommerce_product_options', 12 );

	function lg_woocommerce_product_options() {
	    global $product;
	 	$product_colors = get_field('product_colors');
	 	$product_shapes = get_field('product_shapes');
	 	$product_length = get_field('product_length');
	 	$product_cap = get_field('product_cap');
	 	$product_use = get_field('product_use');
	 	$product_surface = get_field('product_surface');
	    ?>
	    <div class="product-options">

	    	<!-- Colors -->
		    <?php if($product_colors && is_array($product_colors) && sizeof($product_colors) > 0): ?>
		    	<div class="color py-2">
		    		<div class="label mb-1"><strong>Color</strong></div>
			    	<div class="color-list">
			    		
				    	<?php foreach ($product_colors as $key => $color): 
				    		$color_image = get_field('color_image', $color->ID);
				    		$title = get_the_title($color->ID);
				    	?>
				    		<div class="single-color" index="<?php echo $key; ?>" value="<?php echo $title; ?>"><img src="<?php echo $color_image['url']; ?>" alt="<?php echo $color_image['alt']; ?>"></div>
				    	<?php endforeach; ?>
			    	</div>
		    	</div>
		    <?php endif; ?>
		    <!-- end Colors -->

	    	<table class="variations" style="width: 100%;" cellspacing="0">
	    		<tbody>
	
					<!-- Colors Hidden -->
	    			<?php if($product_colors && is_array($product_colors) && sizeof($product_colors) > 0): ?>
		    			<tr class="d-none">
		    				<td class="label">
		    					<label for="color">Color</label>
		    				</td>
		    				<td class="value">
		    					<input id="color" name="_color" type="text">
		    				</td>
		    			</tr>
		    		<?php endif; ?>
		    		<!-- end Colors Hidden -->

					
	    			<!-- Shapes -->
	    			<?php if($product_shapes && is_array($product_shapes) && sizeof($product_shapes) > 0): ?>
	    			<tr>
	    				<td class="label">
	    					<label for="shape">Shape</label>
	    				</td>
	    				<td class="value">
	    					<select id="shape" name="_shape">
	    						<option value="-1"></option>
	    						<?php foreach ($product_shapes as $key => $shape): ?>
	    							<option value="<?php echo get_the_title($shape->ID); ?>"><?php echo get_the_title($shape->ID); ?></option>
	    						<?php endforeach; ?>
	    					</select>
	    				</td>
	    			</tr>
	    			<?php endif; ?>
	    			<!-- End Shapes -->

	    			<!-- length -->
	    			<?php if($product_length && is_array($product_length) && sizeof($product_length) > 0): ?>
	    			<tr>
	    				<td class="label">
	    					<label for="length">Length</label>
	    				</td>
	    				<td class="value">
	    					<select id="length" name="_length">
	    						<option value="-1"></option>
	    						<?php foreach ($product_length as $key => $length): ?>
	    							<option value="<?php echo get_the_title($length->ID); ?>"><?php echo get_the_title($length->ID); ?></option>
	    						<?php endforeach; ?>
	    					</select>
	    				</td>
	    			</tr>
	    			<?php endif; ?>
	    			<!-- End length -->

	    			<!-- cap -->
	    			<?php if($product_cap && is_array($product_cap) && sizeof($product_cap) > 0): ?>
	    			<tr>
	    				<td class="label">
	    					<label for="cap">Cap</label>
	    				</td>
	    				<td class="value">
	    					<select id="cap" name="_cap">
	    						<option value="-1"></option>
	    						<?php foreach ($product_cap as $key => $cap): ?>
	    							<option value="<?php echo get_the_title($cap->ID); ?>"><?php echo get_the_title($cap->ID); ?></option>
	    						<?php endforeach; ?>
	    					</select>
	    				</td>
	    			</tr>
	    			<?php endif; ?>
	    			<!-- End cap -->

	    			<!-- use -->
	    			<?php if($product_use && is_array($product_use) && sizeof($product_use) > 0): ?>
	    			<tr>
	    				<td class="label">
	    					<label for="use">Use</label>
	    				</td>
	    				<td class="value">
	    					<select id="use" name="_use">
	    						<option value="-1"></option>
	    						<?php foreach ($product_use as $key => $use): ?>
	    							<option value="<?php echo get_the_title($use->ID); ?>"><?php echo get_the_title($use->ID); ?></option>
	    						<?php endforeach; ?>
	    					</select>
	    				</td>
	    			</tr>
	    			<?php endif; ?>
	    			<!-- End use -->

	    			<!-- surface -->
	    			<?php if($product_surface && is_array($product_surface) && sizeof($product_surface) > 0): ?>
	    			<tr>
	    				<td class="label">
	    					<label for="surface">Surface</label>
	    				</td>
	    				<td class="value">
	    					<select id="surface" name="_surface">
	    						<option value="-1"></option>
	    						<?php foreach ($product_surface as $key => $surface): ?>
	    							<option value="<?php echo get_the_title($surface->ID); ?>"><?php echo get_the_title($surface->ID); ?></option>
	    						<?php endforeach; ?>
	    					</select>
	    				</td>
	    			</tr>
	    			<?php endif; ?>
	    			<!-- End surface -->

	    			<!-- Quantity -->
	    			<tr class="quantity_field">
	    				<td class="label">
	    					<label for="quantity">Quantity</label>
	    				</td>
	    				<td class="value">

	    				</td>
	    			</tr>
	    			<!-- end Quantity -->

	    		</tbody>
	    	</table>

	    	<div class="single-product-error-message"></div>
	    	
	    	<hr>
	    	</div>
	    <?php
	}

	// --

	global $product_fields;
	$product_fields = ['color', 'shape', 'length', 'cap', 'use', 'surface'];

	function lg_add_cart_item_data( $cart_item, $product_id ){
	 	global $product_fields;

	 	foreach ($product_fields as $key => $field) {
	 		if( isset( $_POST['_'.$field] ) ) {
		        $cart_item[$field] = sanitize_text_field( $_POST['_'.$field] );
		    }
		    isset( $_POST['_'.$field] );
	 	}
	 
	    return $cart_item;
	 
	}
	add_filter( 'woocommerce_add_cart_item_data', 'lg_add_cart_item_data', 10, 2 );

	function lg_get_cart_item_from_session( $cart_item, $values ) {
	 	global $product_fields;

	 	foreach ($product_fields as $key => $field) {
		    if ( isset( $values[$field] ) ){
		        $cart_item[$field] = $values[$field];
		    }
		    //var_dump($values[$field]);
	 	}
	 
	    return $cart_item;
	 
	}
	add_filter( 'woocommerce_get_cart_item_from_session', 'lg_get_cart_item_from_session', 20, 2 );

    function lg_add_order_item_meta( $item_id, $item, $order_id ) {

        global $product_fields;
        $values = $item->legacy_values;
        foreach ($product_fields as $key => $field) {
            if ( ! empty( $values[$field] ) ) {
                wc_add_order_item_meta( $item_id, $field, $values[$field] );
            }
        }
    }
    add_action( 'woocommerce_new_order_item', 'lg_add_order_item_meta', 10, 3 );

	function lg_get_item_data( $other_data, $cart_item ) {
	 
	 	global $product_fields;

	 	foreach ($product_fields as $key => $field) {
		    if ( isset( $cart_item[$field] ) ){
		 
		        $other_data[] = array(
		            'name' => __( $field, 'lg-plugin-textdomain' ),
		            'value' => sanitize_text_field( $cart_item[$field] )
		        );
		 
		    }
	 	}
	 
	    return $other_data;
	 
	}
	add_filter( 'woocommerce_get_item_data', 'lg_get_item_data', 10, 2 );

	/*function lg_email_order_meta_fields( $fields ) { 
	    $fields['custom_field'] = __( ', 'lg-plugin-textdomain' ); 
	    return $fields; 
	} 
	add_filter('woocommerce_email_order_meta_fields', 'lg_email_order_meta_fields');*/

	function lg_order_again_cart_item_data( $cart_item, $order_item, $order ){

		global $product_fields;

	 	foreach ($product_fields as $key => $field) {
			if( isset( $order_item[$field] ) ){
		        $cart_item_meta[$field] = $order_item[$field];
		    }
	 	}
	 
	    return $cart_item;
	 
	}
	add_filter( 'woocommerce_order_again_cart_item_data', 'lg_order_again_cart_item_data', 10, 3 );

?>