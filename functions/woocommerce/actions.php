<?php

	add_action('add_meta_boxes', 'remove_short_description', 999);

	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );

	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_product_thumbnail_before', 9 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_product_thumbnail_after', 13 );

	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_product_button_before', 11 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_product_button_after', 13 );

	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_view_product', 12 );

	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	add_action( 'woocommerce_after_single_product_summary', 'lg_woocommerce_other_products', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	add_action( 'woocommerce_single_product_summary', 'lg_woocommerce_product_short_description', 12 );
	add_action( 'woocommerce_after_single_product_summary', 'lg_woocommerce_single_product_details', 19 );

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 11 );

	add_action('woocommerce_add_to_cart_redirect', 'resolve_dupes_add_to_cart_redirect');
	
	function lg_woocommerce_template_loop_product_thumbnail_before(){
		echo '<div class="lg-woo-product-thumbnail">';
	}

	function lg_woocommerce_template_loop_product_thumbnail_after(){
		echo '</div>';
	}

	function lg_woocommerce_template_loop_product_button_before(){
		echo '<div class="product_button">';
	}

	function lg_woocommerce_template_loop_product_button_after(){
		echo '</div>';
	}

	function lg_woocommerce_template_loop_view_product(){
		global $product;
		echo "<a class='view-product' href='" . get_permalink($product->get_id()) . "'>View Item</a>";
	}

	function lg_woocommerce_other_products(){
		get_template_part( '/templates/template-parts/page/other-products' );
	}

	function lg_woocommerce_single_product_details(){
		get_template_part( '/templates/template-parts/page/single-product-details' );
	}

	function remove_short_description() {

		remove_meta_box( 'postexcerpt', 'product', 'normal');

	}

	function lg_woocommerce_product_short_description(){
		echo get_field('short_description');
	}
	


	function resolve_dupes_add_to_cart_redirect($url = false) {

	     if(!empty($url)) { return $url; }

	     return get_bloginfo('wpurl').add_query_arg(array(), remove_query_arg('add-to-cart'));

	}

?>