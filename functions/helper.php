<?php

# Help functions for the theme

// Format Phone Numbers
function format_phone($phone, $format='hyphen') {

	// This function will format north american phone numbers.
	// 3 formats available. hyphen is set as default.

		// hypen    = 604-555-1234
		// Brackets = (604) 555-1234
		// dots     = 604.555.1234

	$phone = preg_replace("/[^0-9]/", "", $phone);
	$length = strlen($phone);
	$format = strtolower($format);

	// Default phone with brackets
	if ($format == 'brackets') {

		switch($length){

			case 7:
				return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
			break;

			case 10:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
			break;

			case 11:
				return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1 ($2) $3-$4", $phone);
			break;

			default:
				return $phone;
			break;
		}

	}

	// Default phone with hyphen
	if ($format == 'hyphen') {

		switch($length){

			case 7:
				return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
			break;

			case 10:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
			break;

			case 11:
				return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3-$4", $phone);
			break;

			default:
				return $phone;
			break;
		}

	}


	// Default phone with dots
	if ($format == 'dots') {

		switch($length){

			case 7:
				return preg_replace("/([0-9]{3})([0-9]{4})/", "$1.$2", $phone);
			break;

			case 10:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1.$2.$3", $phone);
			break;

			case 11:
				return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1.$2.$3.$4", $phone);
			break;

			default:
				return $phone;
			break;
		}

	}
}



function short_string($string, $limit){
	$chars = preg_replace( "/\r|\n/", " ", $string );
	$chars = explode(' ',strip_tags($chars));
	$chars = array_slice($chars, 0, $limit);

	return join(" ", $chars);
}





function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {

 // remove layout title from text
 $title = '';
 $new_title = get_sub_field('block_title');
 if($new_title){
   return $new_title;
 }else{
   return $title;
 }

}

// name
add_filter('acf/fields/flexible_content/layout_title', 'my_acf_flexible_content_layout_title', 10, 4);



?>
