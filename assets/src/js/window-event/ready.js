// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('mouseover', function(e){
          $(this).siblings('.dropdown-menu').addClass('show');
        });

        $('.dropdown-toggle').on('mouseleave', function(e){
          $(this).siblings('.dropdown-menu').removeClass('show');
        });

        $('.feature-slider').slick({
          autoplay: true,
          fade: true,
          arrows: false,
          dots: false,
          autoplaySpeed: 6000,
          adaptiveHeight: true
        });

        lightbox.option({
          'resizeDuration': 700,
          'wrapAround': true,
          'disableScrolling': true,
          'positionFromTop': 0
        })

        //Mega Menu
        if($('.mega-menu-toggle')[0]){
          $('.mobile-toggle').on('click', function(){
            $('.mega-menu-toggle').click();
          });
        }

        $('.product_button').mouseover(function(){
          $(this).addClass('hover');
        });

        $('.product_button').mouseleave(function(){
          $(this).removeClass('hover');
        });

        $('.product-options select').selectric();
        $('select.orderby').selectric();

        $('.our-testimonials-loop').masonry({
          // options
          itemSelector: '.our-testimonials-loop >div'
        });

        //Woocommerce

        if($('.single-product-top')[0]){
          var quantity = $('.input-text.qty');
          $('.quantity_field .value').append(quantity);

          $('.single-color img').on('click', function(){
            var value = $(this).closest('.single-color').attr('value');
            if($('.variations input[name=_color]').val() != value){
              $('.variations input[name=_color]').val(value);
              $(this).closest('.single-color').addClass('active').siblings().removeClass('active');
            }
          });

          $('form.cart').submit(function(){
            var color = $('.product-options').find('input[name=_color]');
            var shape = $('.product-options').find('select[name=_shape]');
            var length = $('.product-options').find('select[name=_length]');
            var cap = $('.product-options').find('select[name=_cap]');
            var use = $('.product-options').find('select[name=_use]');
            var surface = $('.product-options').find('select[name=_surface]');

            var check_list = [
              {'name': 'Color', 'value': color},
              {'name': 'Shape', 'value': shape},
              {'name': 'Length', 'value': length},
              {'name': 'Cap', 'value': cap},
              {'name': 'Use', 'value': use},
              {'name': 'Surface', 'value': surface}
            ];

            var error_message = '<hr><div class="single_product_error_message text-danger">';
            var success = true;

            check_list.map(function(item){
              if(item.value[0]){
                if(!item.value.val() || item.value.val() == '' || item.value.val() == -1){
                  error_message += '<div><strong>' + item.name + '</strong> is required.</div>';
                  success = false;
                }
              }
            });

            error_message += '</div>';

            if(success)
              return true;
            else
              $('.single-product-error-message').html(error_message);
              return false;
          });
        }

              $(".scroll-top").click(function() { 
        var scrollTo = $("#masthead");
        var settings = {
          duration: 2 * 1000,
          offset: 0
        };

        KCollection.headerScrollTo(scrollTo, settings);   
        
        });
        
    });

}(jQuery));