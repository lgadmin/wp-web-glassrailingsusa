<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
// @codingStandardsIgnoreStart
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header">
    <div class="header-content">

      <?php get_template_part("/templates/template-parts/header/site-utility-bar"); ?>

      <div class="header-main container">
    		<div class="site-branding">
          <div class="logo">
            <?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>
          </div>
          <div class="header-cart-item d-flex d-md-none"><div class="shopping-cart-icon"><a href="' . wc_get_cart_url() . '"><i class="fas fa-shopping-cart"></i> Loading... </a></div></div>
          <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
    		</div><!-- .site-branding -->

        <div class="site-utility d-none d-md-flex">
          

          <div class="header-call d-none d-md-block px-1">
            <div class="h4 text-primary mb-0"><strong>CALL TODAY</strong></div>
            <a class="h4 text-secondary" href="tel:<?php echo the_field('contact_1_phone', 'options'); ?>"><span><?php echo the_field('contact_1_name', 'options'); ?></span><span><?php echo the_field('contact_1_phone', 'options'); ?></span></a><br>
            <a class="h4 text-secondary" href="tel:<?php echo the_field('contact_2_phone', 'options'); ?>"><span><?php echo the_field('contact_2_name', 'options'); ?></span><span><?php echo the_field('contact_2_phone', 'options'); ?></span></a><br>
            <a class="h4 text-secondary" href="tel:<?php echo the_field('contact_3_phone', 'options'); ?>"><span><?php echo the_field('contact_3_name', 'options'); ?></span><span><?php echo the_field('contact_3_phone', 'options'); ?></span></a><br>
            <a class="h4 text-secondary" href="tel:<?php echo the_field('contact_4_phone', 'options'); ?>"><span><?php echo the_field('contact_4_name', 'options'); ?></span><span><?php echo the_field('contact_4_phone', 'options'); ?></span></a>
         
           
          </div>
          <div class="header-cart-item"><div class="shopping-cart-icon"><a href="' . wc_get_cart_url() . '"><i class="fas fa-shopping-cart"></i> Loading... </a></div></div>

        </div>
        
      </div>

     

        <div class="main-navigation bg-primary">
          <div class="container">
              <nav class="navbar navbar-default navbar-expand-md">  
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>


                <!-- Main Menu  -->
                <?php 

                  $mainMenu = array(
                    // 'menu'              => 'menu-1',
                    'theme_location'    => 'top-nav',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'main-navbar',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'            => new WP_Bootstrap_Navwalker()
                  );
                  wp_nav_menu($mainMenu);

                ?>
            </nav>
          </div>
      </div>
    </div>

    <?php get_template_part("/templates/template-parts/page/feature-slider"); ?>

    <div class="scroll-top">
      <i class="fas fa-chevron-up"></i>
    </div>

  </header><!-- #masthead -->
